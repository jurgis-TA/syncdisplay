import logging
import uasyncio as asyncio
import arequests as requests
import asyn
import json
import utime
import socket
from core.constants import *

# Options
REFRESH_INTERVAL = 30 * 1 # refresh every 10 sec
LAYOUT_FILE = "/app/layout.json"
DEVICE_ID = 'SM00ZZZZ0'

USER_EMAIL = 'antanas@tableair.com'
USER_PASSWORD = '1LoveTableAir'

LOGIN_REQUEST = '''
{
    "Accept": "application/json",
    "Content-Type": "application/json",
    "Accept-Language": "en-US,en;q=0.5",
    body: {
        "email": "antanas@tableair.com",
        "password": "1LoveTableAir",
        "type": 1
    }
}
'''

DUMMY_RESPONSE = '''
[{
    "id": 1372,
    "room": "SM00ZZZZ0",
    "host_first_name": null,
    "host_last_name": null,
    "participants": [],
    "start": "2019-06-27T05:42:00Z",
    "end": "2019-06-27T05:52:00Z",
    "date_created": "2019-06-27T05:42:45.987516Z",
    "title": "Meeting (auto booked)",
    "status": 1,
    "should_notify": false,
    "host": 0
}]
'''

class App:
    # User App of Hub SDK, send a 'Hello world' to wireless display

    def __init__(self, mgr, loop, pan):
        self.log = logging.getLogger("APP")
        self.log.setLevel(logging.DEBUG)
        self.pan = pan
        self.loop = loop
        self.targetNodeId = None
        self.nodeOnlineEvent = asyn.Event()
        mgr.setPanCallback(self.onPanEvent)
        self.loop.create_task(self.task()) # run a asyncio task
        self.log.info('App Starting')


    async def task(self):
        # waiting for at least one display node becomes online
        await self.nodeOnlineEvent
        self.targetNodeId = self.nodeOnlineEvent.value()
        self.nodeOnlineEvent.clear()

        # loop forever
        while True:
            self.log.debug('App Task Running')
            # await self._authenticateTableair()
            await self._fetchActivity()
            await asyncio.sleep(REFRESH_INTERVAL) # sleep for a while


    def onPanEvent(self, event, data):
        if event == EVT_NODE_PRESENCE:
            self.onNodePresence(data['nodeId'], data['isOnline'])


    def onNodePresence(self, nodeId, isOnline):
        """Event of presence/absence
        Whenever a node becomes online or offline, this method will be called by system
        Args:
            nodeId: (int) indicates which node is changing its status
            isOnline: (bool) =True, a presence; =False: absence
        """
        self.log.info('node "%s" is %s', hex(nodeId), 'online.' if isOnline else 'offline!')
        if isOnline:
            self.nodeOnlineEvent.set(nodeId)


    async def _authenticateTableair(self):
        url = 'http://api.tableair.lt/auth/'
        self.log.debug(url)
        self.log.debug(LOGIN_REQUEST)
        response = await requests.post(url, json = LOGIN_REQUEST)
        self.log.debug(str(response._cached))
        self.log.debug(response.status_code)
        self.log.debug(response.reason)
        if response:
            await response.close()


    async def _fetchActivity(self):
        """Lookup current weather info through a REST API request"""
        url = 'https://api.tableair.lt/rooms/{deviceID}/booking/'.format(deviceID = DEVICE_ID)
        self.log.debug(url)
        response = None
        try:
            current_time = utime.time()
            response = await requests.get(url)
            self.log.debug('Current time: {time}'.format(time = current_time))
            if response.status_code == 200:
                # res = await response.json()
                self.log.debug(response.status_code)
                self.log.debug(str(response._cached))
                self.log.debug(str(response))
            else:
                self.log.debug('Bad response. Error {http_error}'.format(http_error = response.status_code))
            #Override output
            response._cached = DUMMY_RESPONSE
            res = await response.json()
            self.log.debug(res)
            # Parse server response
            await self._parseMeetingRoom(res)
        except Exception as e:
            self.log.exception(e, 'unable to get activity')
            if response:
                self.log.debug(str(response._cached))
        finally:
            if response: # If the error is an OSError the socket has to be closed.
                await response.close()


    async def _parseMeetingRoom(self, response):
        try:
            time_start = response[0]['start']
            time_end = response[0]['end']
            mr_title = response[0]['title']
            self.log.info('Meeting room title: {title}'.format(title = mr_title))
            self.log.info('Booked from {start} to {end}'.format(start = time_start, end = time_end))
            await self._sendToDisplay(mr_title, time_start, time_end)
        except Exception as e:
            self.log.exception(e, 'unable to parse response')

    async def _sendToDisplay(self, title, start, end):
        if not self.pan:
            return False
        self.log.debug('send to display')
        try:
            with open(LAYOUT_FILE, 'r') as f:
                layout = json.loads(f.read())

                import core.device_display as disp
                dp = disp.getSpec(disp.DISPLAY_4IN2_BW)
                fullWidth = dp['resolution']['width']
                from core.font_render import FontRender
                self.font = FontRender()

                # Replace title
                # width = self.font.getRenderedWidth(title, self.font.FONT_YKSZ_BOLD_44) # calculate width, in order to align center
                layout['items'][0]['data']['caption'] = title
                # layout['items'][0]['data']['offset']['x'] = (fullWidth - width - 48 * 2) // 2

                # Replace start time
                # width = self.font.getRenderedWidth(start, self.font.FONT_DIN_CON_32) # calculate width, in order to align center
                layout['items'][1]['data']['caption'] = start
                # layout['items'][1]['data']['offset']['x'] = (fullWidth - width - 48 * 2) // 2

                # Replace end time
                layout['items'][2]['data']['caption'] = end

                return await self.pan.updateDisplay(self.targetNodeId, layout)
        except Exception as e:
            self.log.exception(e, 'unable to process layout')
        return False